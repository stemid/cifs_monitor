import requests

class MonitorscoutReporter(object):

    def __init__(self, **kw):
        config = kw.get('config')
        self.report_url = '{api}/monitors/passive/{monitor_id}/report'.format(
            api=config.get('monitorscout', 'api_url'),
            monitor_id=config.get('monitorscout', 'monitor_id')
        )
        
        self.report_headers = {
            'X-Auth-API-Key': config.get('monitorscout', 'api_key'),
            'Content-type': 'application/json'
        }
    
    def report(self, status):
        report_data = {
            'value': status
        }
        r = requests.put(
            self.report_url,
            headers=self.report_headers,
            json=report_data
        )
        return r