# CIFS monitoring

The goal here is to run on both Windows and Linux with this code.

And to be able to plugin different reporting mechanisms for different monitoring backends.

So far only one monitoring backend is supported, a proprietary Monitorscout backend used by my current employer.

# Windows

## Step 1: Download Python 3 for Windows

From [here](https://www.python.org/downloads/windows/), pick the latest stable release and download the web installer package.

## Step 2: Install Python 3 on Windows

Select "Add Python to PATH" and click "Install Now". Follow the instructions, you need to be administrator.

## Step 3: Setup python virtualenv

**Unfortunately I ran into issues with venv on Windows 2008r2 that I have not resolved yet but this guide worked on my Windows 7 client.**

Open cmd.exe and go to the directory this git repository is, in my case it was C:\Users\administrator\Downloads\cifs_monitor-master so I typed in the following.

    cd "C:\Users\administrator\Downloads\cifs_monitor-master"

Create a virtualenv like this;

    python -m venv venv

Install requirements in the virtualenv like this;

    .\venv\Scripts\pip install -r requirements.txt

## Step 3a: Global environment

On Windows 2008r2 I was able to use a global python environment. Replace the two last commands with this.

    pip install -r requirements.txt

## Step 4: Configure cifs_monitor.cfg

This should be self-explanatory or commented.

## Step 5: Test run script from cmd

With virtualenv it's;

    .\venv\Scripts\python cifs_monitor.py

Globally it's;

    python cifs_monitor.py

Read the help text and add the arguments that are necessary.

# Linux

## Step 1: Ensure you have Python 3 installed on your system

Follow the package manager instructions for your distro. For example;

    sudo apt-get install python3 # Debian-based
    sudo yum install python3 # RHEL-based

## Step 2: Create virtualenv

Go to the repository directory.

    python3 -m venv venv

Activate the virtualenv for the rest of the steps.

    source venv/bin/activate

## Step 3: Install requirements

Stay in the downloaded git directory.

    pip install -r requirements.txt

## Step 4: Configure cifs_monitor.cfg

Should be self-explanatory, this file holds credentials so ensure you protect it.

## Step 5: Test run script from cli

    python cifs_monitor.py

Read the help text and add necessary arguments.