#!/usr/bin/env python

import sys
from socket import gethostname
from argparse import ArgumentParser, FileType
from configparser import RawConfigParser

from smb.SMBConnection import SMBConnection
from smb.smb_structs import OperationFailure

from cifsmonitorlib.report import MonitorscoutReporter
from cifsmonitorlib.logger import AppLogger

parser = ArgumentParser()
parser.add_argument(
    '-v', '--verbose',
    action='count',
    default=False,
    dest='verbose',
    help='Verbose output, use more v\'s to increase level'
)

parser.add_argument(
    '-c', '--config',
    type=FileType('r'),
    required=True,
    dest='config',
    help='Configuration file with SMB credentials'
)

parser.add_argument(
    '-s', '--server',
    required=True,
    dest='server',
    help='CIFS server name or IP'
)

parser.add_argument(
    '-p', '--port',
    dest='port',
    default=139,
    help='CIFS port (default: 139)'
)

parser.add_argument(
    '-m', '--share',
    required=True,
    dest='share',
    help='Name of share to use on CIFS server'
)

parser.add_argument(
    '--use-ntlm-v2',
    action='store_true',
    default=True,
    help='Use NTLMv2 with pysmb'
)

parser.add_argument(
    '--directory',
    default='Monitoring_Test',
    help='Directory to create/look for on CIFS share'
)

parser.add_argument(
    '--test-string',
    dest='test_string',
    default='Testing, testing, hello?',
    help='Test string to send as echo command to CIFS server'
)

parser.add_argument(
    '--timeout',
    type=int,
    default=10,
    help='Timeout value for CIFS connection in seconds'
)

args = parser.parse_args()

config = RawConfigParser()
config.readfp(args.config)

l = AppLogger(name='cifs-monitor', config=config).logger

client_hostname = gethostname()

reporter = MonitorscoutReporter(config=config)

try:
    smbconn = SMBConnection(
        config.get('cifs', 'username'),
        config.get('cifs', 'password'),
        client_hostname,
        args.server,
        use_ntlm_v2=args.use_ntlm_v2
    )
except Exception as e:
    reporter.report(False)
    if args.verbose:
        l.error(str(e))
    sys.exit(1)

if not smbconn.connect(args.server, args.port):
    reporter.report(False)
    if args.verbose:
        l.error('Connection to {server}:{port} failed'.format(
            server=args.server,
            port=args.port
        ))
    sys.exit(1)

response = smbconn.echo(args.test_string, timeout=args.timeout)

if response != args.test_string:
    reporter.report(False)
    if args.verbose:
        l.error('Something went wrong when echo-testing CIFS server')
    sys.exit(1)

results = []
# This listPath call will always fail on a first run because the pattern
# won't match.
try:
    results = smbconn.listPath(
        args.share,
        '/',
        pattern=args.directory
    )
except OperationFailure as e:
    if args.verbose > 1:
        l.info(str(e))
    pass

if not results:
    smbconn.createDirectory(args.share, args.directory)

results = smbconn.listPath(
    args.share,
    '/',
    pattern=args.directory
)

if not results:
    reporter.report(False)
    if args.verbose:
        l.error('Could not find directory {dir}'.format(dir=args.directory))
    sys.exit(1)

smbconn.deleteDirectory(args.share, args.directory)

smbconn.close()

res = reporter.report(True)
if args.verbose:
    l.info('Report result: {res}: {output}'.format(
        res=res.status_code,
        output=res.text
    ))

sys.exit(0)